'use strict'
var bcrypt = require('bcrypt-nodejs');
var mongoosePaginate = require('mongoose-pagination');
var fs = require('fs');
var path = require('path');

var User = require('../models/user');
var Follow = require('../models/follow');
var Publication = require('../models/publication');

var jwt = require('../services/jwt');
/**
 * Funciones de prueba
 */
function home(req, res) {
    res.status(200).send({
        message: 'Hola mundo desde el servidor de nodeJS'
    });
}

function pruebas(req, res) {
    // console.log(req.body)
    res.status(200).send({
        message: 'Accion de pruebas en el servidor de nodeJS'
    });
}

/**
 * Registro de usuarios
 */
function saveUser(req, res) {
    var params = req.body;
    var user = new User();

    if (params.name && params.surname &&
        params.nick && params.email && params.password) {

        user.name = params.name;
        user.surname = params.surname;
        user.nick = params.nick;
        user.email = params.email;
        user.role = "ROLE_USER";
        user.image = null;

        User.find({
            $or: [
                { email: user.email.toLowerCase() },
                { nick: user.nick.toLowerCase() }
            ]
        }).exec((err, users) => {
            if (err) return res.status(500).send({ message: 'Error al guardar el usuario' });
            if (users && users.length >= 1) {
                return res.status(500).send({ message: 'Ya existe este email/usuario' });
            } else {
                bcrypt.hash(params.password, null, null, (err, hash) => {
                    user.password = hash;
                    user.save((err, userStored) => {
                        if (err) return res.status(500).send({ code: 500, message: 'Error al guardar el usuario', params: params, user: user });

                        if (userStored) {
                            res.status(200).send({ user: userStored });
                        } else {
                            res.status(404).send({ message: 'No se ha registrado el usuario' });
                        }

                    })
                });
            }
        });



    } else {
        res.status(200).send({
            message: 'Faltan datos, por favor revisa el formulario'
        });
    }
}

/**
 * Login de usuarios
 */
function loginUser(req, res) {
    var params = req.body;

    var email = params.email;
    var password = params.password;

    User.findOne({
            email: email
        },
        (err, user) => {

            if (err) return res.status(500).send({ message: "Error en la peticion" });
            if (user) {
                bcrypt.compare(password, user.password, (err, check) => {
                    if (check) {
                        if (params.gettoken) {
                            // generar y devolver token
                            return res.status(200).send({
                                token: jwt.createToken(user)
                            });
                        } else {
                            // devolver datos de usuario
                            user.password = undefined;
                            return res.status(200).send({ user });
                        }
                    } else {
                        return res.status(404).send({ message: "El usuario no se ha podido identificar" });
                    }
                });
            } else {
                return res.status(404).send({ message: "No se ha encontrado ningun usuario con estos datos" });
            }
        }
    );

}


/**
 * Conseguir datos de un usuario
 */

function getUser(req, res) {
    var userId = req.params.id;
    User.findById(userId, (err, user) => {
        if (err) return res.status(500).send({ message: "Error en la petición" });
        if (!user) return res.status(404).send({ message: "Usuario no encontrado" });

        user.password = undefined;

        followThisUser(req.user.sub, userId).then( (value) =>{
            user.password = undefined;
            return res.status(200).send({ 
                user, 
                following: value.following, 
                followed: value.followed
            });
        });
    });
}

async function followThisUser(identity_user_id, user_id){
    try{
        var following = await Follow.findOne({ "user": identity_user_id, "followed": user_id })
                                    .exec()
                                    .then((following) => {    
                                        return following;
                                    })
                                    .catch((err) => {
                                        return handleError(err);
                                    }); 
        
        var followed = await Follow.findOne({ "user": user_id, "followed": identity_user_id })
                                    .exec()
                                    .then((followed) => {
                                        return followed;
                                    })
                                    .catch((err) => {
                                        return handleError(err);
                                    });
        return { 
            following : following,
            followed  : followed
        }
    }catch(e){
        console.log(e);
    }

}

/**
 * Conseguir un listado de usuarios
 */
function getUsers(req, res) {
    var identity_user_id = req.user.sub;
    var page = 1;

    if (req.params.page) {
        page = req.params.page;
    }

    var itemsPerPage = 5;

    //User.find().paginate(page, itemsPerPage, (err, users, total) => {
    User.find().limit(itemsPerPage).skip((page - 1) * itemsPerPage).exec(function(err, users) {
        User.count().exec(function(err, total) {
            if (err) return res.status(500).send({ message: "Error en la petición" });
            if (!users) return res.status(404).send({ message: "No hay usuarios disponibles " });

            followUserIds(identity_user_id).then((value) =>{
                return res.status(200).send({
                    myself: identity_user_id,
                    users,
                    following: value.following,
                    followed: value.followed,
                    total,
                    pages: Math.ceil(total / itemsPerPage)
                });
            });
        });
    });
}

async function followUserIds(user_id){
    try{
        var following = await Follow.find({ 'user': user_id })
                              .select({ '_id':0, '__v':0, 'user':0 })
                              .exec()
                              .then((following) => {
                                  return following;
                              })
                              .catch((err) => {
                                  return handleError(err);
                              });
        var following_clean = [];
        following.forEach((follow)=>{
            following_clean.push(follow.followed);
        });
    }catch (e){
        console.log(e);
    }
    try{
        var followed = await Follow.find({ 'followed': user_id })
                              .select({ '_id':0, '__v':0, 'followed':0 })
                              .exec()
                              .then((followed) => {
                                return followed;
                              })
                              .catch((err) => {
                                  return handleError(err);
                              });
        var followed_clean = [];
        followed.forEach((follow) => {
            followed_clean.push(follow.user);
        });
    }catch (e){
        console.log(e);
    }
    
    return { 
        following : following_clean,
        followed  : followed_clean
    }

    
}
/**
 * Edicion de datos de usuario
 */
function updateUser(req, res) {
    var userId = req.params.id;
    var update = req.body;

    // borrar propiedad password
    delete update.password;

    if (userId != req.user.sub) return res.status(401).send({ message: "No tienes privilegios para modificar este usuario" });

    User.findByIdAndUpdate(userId, update, { new: true }, (err, userUpdated) => {
        if (err) return res.status(500).send({ message: 'Error en la petición' });
        if (!userUpdated) return res.status(404).send({ message: 'No se ha actualizado el usuario' });

        return res.status(200).send({ user: userUpdated });
    });
}

/**
 * Subir archivos de avatar de usuarios
 */
function uploadAvatar(req, res) {
    var userId = req.params.id;

    if (req.files) {
        var file_path = req.files.image.path;
        // console.log(file_path);
        var file_split = file_path.split('\/');
        // console.log(file_split);

        var file_name = file_split[2];
        // console.log(file_name);

        var file_ext = file_name.split('\.')[1];
        // console.log(file_ext);
        
        if (userId != req.user.sub){
            return removeErroFile(res, file_path, "Permisos insuficientes");
        } 

        if (file_ext == 'png' || file_ext == 'jpg' || file_ext == 'jpeg' || file_ext == 'gif') {
            // Actualizar documento de usuario logeado

            User.findByIdAndUpdate(userId, {image: file_name},{new:true}, (err, userUpdated) => {
                if (err) return res.status(500).send({ message: 'Error en la petición' });
                if (!userUpdated) return res.status(404).send({ message: 'No se ha actualizado el usuario' });

                return res.status(200).send({ user: userUpdated });
            });
        } else {
            return removeErroFile(res,file_path, "Extension no válida")
        }

    } else {
        return res.status(200).send({ message: 'No se han subido imagenes' });
    }
}

/**
 * Devuelve el avatar del usuario si existe
 * @param {*} req 
 * @param {*} res 
 */
function getImageFile(req, res){
    var image_file = req.params.imageFile;
    var path_file = './uploads/users/'+image_file;

    fs.exists(path_file, (exists) => {
        if(exists){
            res.sendFile(path.resolve(path_file));
        }else{
            res.status(404).send({ 
                path: path_file,
                message: 'No existe la imagen' 
            });
        }
    });
}

function removeErroFile(res,file_path, msg){
    fs.unlink(file_path, (err) => {
        return res.status(200).send({ message: msg})
    });
}

function getCounters(req, res){
    var user_id = req.params.id ? req.params.id : req.user.sub;
    
    
    getCountFollow(user_id).then((value) => {
        return res.status(200).send( value );
    });
    
}

async function getCountFollow(user_id){
    try {
        var following = await Follow.count({'user': user_id})
                                    .exec()
                                    .then((count) => {
                                        return count;
                                    })
                                    .catch((err) => {
                                        return handleError(err);
                                    });
        var followed = await Follow.count({'followed': user_id})
                                    .exec()
                                    .then((count) => {
                                        return count;
                                    })
                                    .catch((err) => {
                                        return handleError(err);
                                    });
        var publications = await Publication.count({ 'user': user_id})
                                            .exec()
                                            .then((count) => {
                                                return count;
                                            })
                                            .catch((err) => {
                                                return handleError(err);
                                            });
        
        return {
            following : following,
            followed : followed,
            publications: publications
        }
    } catch (e) {
        console.log(e)
    }
}

/**
 * Exportaciones
 */
module.exports = {
    home,
    pruebas,
    saveUser,
    loginUser,
    getUser,
    getUsers,
    updateUser,
    uploadAvatar,
    getImageFile,
    getCounters
}