'use strict'

// var path = require('path');
// var fs = require('fs');
var mongoosePaginate = require('mongoose-pagination');

var User = require('../models/user');
var Follow = require('../models/follow');

function saveFollow(req, res){
    var params = req.body;

    var follow = new Follow();
    follow.user = req.user.sub;
    follow.followed = params.followed;

    follow.save((err, followStored) => {
        if(err) return res.status(500).send({ message: "No se ha podido incluir este usuario en tu lista de seguimiento" });
        if(!followStored) return res.status(404).send({ message: "El seguimiento no se ha guardado" });

        return res.status(200).send({ message: "Guardado correcto", follow: followStored });

    });
}

function deleteFollow(req, res){

    var userId = req.user.sub;
    var followId = req.params.id;

    Follow.find({ 'user' : userId, 'followed' : followId }).remove((err) => {
        if(err) return res.status(500).send({ message: "No se ha podido incluir este usuario en tu lista de seguimiento" });
        return res.status(200).send({ message: "Ya no sigues a ese usuario"});
    });
}

function getFollowingUsers(req, res){
    var userId = req.user.sub;
    
    if(req.params.id && req.params.page)
        userId = req.params.id;

    var page = 1;
    if(req.params.page)
        page = req.params.page;
    else
        page = req.params.id;

    var itemsPerPage = 5;
    //Follow.find({ user:userId }).populate({ path: 'followed' }).paginate(page, itemsPerPage, (err, follow);
    Follow.find({ user:userId })
        .populate({ path: 'followed' })
        .limit(itemsPerPage)
        .skip((page - 1) * itemsPerPage)
        .exec(function(err, follows) {
            Follow.count().exec(function(err, total) {
                if (err) return res.status(500).send({ message: "Error en la petición" });
                if (!follows) return res.status(404).send({ message: "No sigues a ningun usuario" });

                return res.status(200).send({
                    follows,
                    total,
                    pages: Math.ceil(total / itemsPerPage)
                });
            });
        });
    
}

function getFollowedUsers(req, res){
    var userId = req.user.sub;
    
    if(req.params.id && req.params.page)
        userId = req.params.id;

    var page = 1;
    if(req.params.page)
        page = req.params.page;
    else
        page = req.params.id;

    var itemsPerPage = 5;
    Follow.find({ followed:userId })
        .populate({ path: 'user' })
        .limit(itemsPerPage)
        .skip((page - 1) * itemsPerPage)
        .exec(function(err, follows) {
            Follow.count().exec(function(err, total) {
                if (err) return res.status(500).send({ message: "Error en la petición" });
                if (!follows) return res.status(404).send({ message: "No te sigue ningun usuario" });

                return res.status(200).send({
                    follows,
                    total,
                    pages: Math.ceil(total / itemsPerPage)
                });
            });
        });
}

/**
 * Usuarios que sigo
 * @param {*} req 
 * @param {*} res 
 */
function getMyFollows(req, res){
    var userId = req.user.sub;
    
    var find = (req.params.followed) ? Follow.find({ followed: userId }) : Follow.find({ user: userId });


    find.populate('user followed').exec((err, follows) => {
        if (err) return res.status(500).send({ message: "Error en la petición" });
        if (!follows || follows.length == 0) return res.status(404).send({ message: (req.params.followed) ? "No te sigue ningun usuario" : "No sigues a ningun usuario" });

        return res.status(200).send({ follows });
    });
}


module.exports = {
    saveFollow,
    deleteFollow,
    getFollowingUsers,
    getFollowedUsers,
    getMyFollows
}