'use strict'

var moment = require('moment');
var mongoosePaginate = require('mongoose-pagination');

var User = require('../models/user');
var Follow = require('../models/follow');
var Message = require('../models/message');

function prueba(req, res){
    res.status(200).send({ message: "Hola desde el controlador de mensajes directos " + req.user.nick });
}

function saveMessage(req, res){
    var params = req.body;

    if(!params.text || !params.receiver){
        res.status(200).send({ message: "Datos necesarios insuficientes, revisa el formulario" });
    }

    var message = new Message();
    message.emitter = req.user.sub;
    message.receiver = params.receiver;
    message.text = params.text;
    message.created_at = moment().unix();
    message.viewed = 'false';

    message.save((err, messageStored) => {
        if(err) return res.status(500).send({ message: "Error en la petición" });
        if(!messageStored) return res.status(404).send({ message: "Error al enviar mensaje" });

        return res.status(200).send({ message: messageStored });
    } );

}

function getReceivedMessages(req, res){
    var userId = req.user.sub;

    var page = req.params.page ? req.params.page :1;

    var itemsPerPage = 4;

    Message.find({ 'receiver': userId })
           .populate( 'emitter', '_id nick image' ) 
           .limit(itemsPerPage)
           .skip((page - 1) * itemsPerPage)
           .exec(function(err, messages) { 
               Message.count({ 'receiver' : userId }).exec(function(err, total) {
                    if(err) return res.status(500).send({ message: "Error en la petición" });
                    if(!messages) return res.status(404).send({ message: "No hay mensajes" });

                    return res.status(200).send({
                        total: total,
                        pages: Math.ceil(total/itemsPerPage),
                        messages
                    });
                });
           });
}

function getEmittedMessages(req, res){
    var userId = req.user.sub;

    var page = req.params.page ? req.params.page :1;

    var itemsPerPage = 4;

    Message.find({ 'emitter': userId })
           .populate( 'receiver', '_id nick image' ) 
           .limit(itemsPerPage)
           .skip((page - 1) * itemsPerPage)
           .exec(function(err, messages) { 
               Message.count({ 'emitter' : userId }).exec(function(err, total) {
                    if(err) return res.status(500).send({ message: "Error en la petición" });
                    if(!messages) return res.status(404).send({ message: "No hay mensajes" });

                    return res.status(200).send({
                        total: total,
                        pages: Math.ceil(total/itemsPerPage),
                        messages
                    });
                });
           });
}

function getUnviewedMessages(req, res){
    var userId = req.user.sub;

    Message.count({ 'receiver' : userId, viewed: 'false' })
           .exec((err, count) => {
                if(err) return res.status(500).send({ message: "Error en la petición" });
                return res.status(200).send({
                    'unviewed': count
                });
           });
}

function setViewedMessages(req, res){
    var userId = req.user.sub;

    Message.update({ 'receiver': userId, viewed: 'false'}, {viewed: 'true'}, {"multi": true}, (err, messagesUpdated) => {
        if(err) return res.status(500).send({ message: "Error en la petición" });
        
        return res.status(200).send({
            messages: messagesUpdated
        });
    });
}

module.exports = {
    prueba,
    saveMessage,
    getReceivedMessages,
    getEmittedMessages,
    getUnviewedMessages,
    setViewedMessages
}