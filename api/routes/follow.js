'use strict'

var express = require('express');
var FollowController = require('../controllers/follow');

var api = express.Router();
var mw_auth = require('../middlewares/authenticated');

//api.get('/', FollowController);
api.post('/follow', mw_auth.ensureAuth, FollowController.saveFollow);
api.delete('/follow/:id', mw_auth.ensureAuth, FollowController.deleteFollow);
api.get('/following/:id?/:page?', mw_auth.ensureAuth, FollowController.getFollowingUsers);
api.get('/followed/:id?/:page?', mw_auth.ensureAuth, FollowController.getFollowedUsers);
api.get('/get-my-follows/:followed?', mw_auth.ensureAuth, FollowController.getMyFollows);

module.exports = api;