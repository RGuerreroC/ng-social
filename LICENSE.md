<p align="center">

![CreativeCommons 4.0](https://creativecommons.org/images/deed/cc-logo.jpg)
![Attribution](https://creativecommons.org/images/deed/by.png)
![No comercial](https://creativecommons.org/images/deed/nc-eu.png)
![Share Alike](https://creativecommons.org/images/deed/sa.png)
# Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)

</p>

This is a human-readable summary of (and not a substitute for) the [license](https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode).

## You are free to:

* __Share__ — copy and redistribute the material in any medium or format

* __Adapt__ — remix, transform, and build upon the material

###### The licensor cannot revoke these freedoms as long as you follow the license terms.

---

### Under the following terms:

* __Attribution__ — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.

* __NonCommercial__ — You may not use the material for commercial purposes.

* __ShareAlike__ — If you remix, transform, or build upon the material, you must distribute your contributions under the same license as the original.

__No additional restrictions__ — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.

---

### Notices:
You do not have to comply with the license for elements of the material in the public domain or where your use is permitted by an applicable exception or limitation.
No warranties are given. The license may not give you all of the permissions necessary for your intended use. For example, other rights such as publicity, privacy, or moral rights may limit how you use the material.