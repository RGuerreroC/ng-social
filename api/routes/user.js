'use strict'

var express = require('express');
var UserController = require('../controllers/user');

var api = express.Router();
var mw_auth = require('../middlewares/authenticated');

var multipart = require('connect-multiparty');
var mw_uploadAv = multipart({uploadDir: './uploads/users'});

api.get('/', UserController.home);
api.get('/pruebas', mw_auth.ensureAuth, UserController.pruebas);
api.post('/register', UserController.saveUser);
api.post('/login', UserController.loginUser)
api.get('/user/:id', mw_auth.ensureAuth, UserController.getUser);
api.get('/users/:page?', mw_auth.ensureAuth, UserController.getUsers);
api.put('/update-user/:id', mw_auth.ensureAuth, UserController.updateUser);
api.post('/upload-avatar/:id', [mw_auth.ensureAuth, mw_uploadAv], UserController.uploadAvatar);
api.get('/get-avatar/:imageFile', UserController.getImageFile);
api.get('/counters/:id?', mw_auth.ensureAuth, UserController.getCounters);

module.exports = api;