'use strict'

var express = require('express');

var PublicationController = require('../controllers/publication');

var api = express.Router();

var mw_auth = require('../middlewares/authenticated');

var multipart = require('connect-multiparty');
var mw_uploadPub = multipart({uploadDir: './uploads/publications'});

api.get('/prueba-pub', mw_auth.ensureAuth, PublicationController.prueba);
api.post('/publication', mw_auth.ensureAuth, PublicationController.savePublication);
api.get('/publications/:page?', mw_auth.ensureAuth, PublicationController.getPublications);
api.get('/publication/:id', mw_auth.ensureAuth, PublicationController.getPublication);
api.delete('/publication/:id', mw_auth.ensureAuth, PublicationController.deletePublication);
api.post('/upload-image-pub/:id', [mw_auth.ensureAuth, mw_uploadPub], PublicationController.uploadFilePub);
api.get('/get-image-pub/:imageFile', PublicationController.getImageFile);

module.exports = api;