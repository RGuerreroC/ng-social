'use strict'

var path = require('path');
var fs = require('fs');
var moment = require('moment');
var mongoosePaginate = require('mongoose-pagination');

var Publication = require('../models/publication');
var User = require('../models/user');
var Follow = require('../models/follow');

function prueba(req, res){
    res.status(200).send({ message: "Hola desde las publicaciones " + req.user.nick  });
}


function savePublication(req, res){
    var params = req.body;
    
    if(!params.text) return res.status(200).send({ message: "Debes escribir algo" });

    var publication = new Publication();
    publication.text = params.text;
    publication.file = params.file;
    publication.user  = req.user.sub;
    publication.created_at = moment().unix();

    publication.save((err, publicationStored) => {
        if(err) return res.status(500).send({ message: 'Error al guardar la publicación' });
        if(!publicationStored) return res.status(404).send({ message: "No se ha guardado la publicación" });

        return res.status(200).send({ publication: publicationStored });
    });
}

function getPublications(req, res){
    var page = req.params.page ? req.params.page : 1;
    
    var itemsPerPage = 4;

    Follow.find({ user: req.user.sub})
          .populate('followed')
          .exec((err, follows) => {
            if(err) return res.status(500).send({ message: 'Error al devolver el seguimiento' });
            var follows_clean = [];
            follows.forEach(follow => {
                follows_clean.push(follow.followed);
            });

            Publication.find({ user: {"$in": follows_clean } })
                       .sort('-created_at')
                       .populate('user')
                       .limit(itemsPerPage)
                       .skip((page - 1) * itemsPerPage)
                       .exec(function(err, publications) {
                           Publication.count().exec(function(err, total) {
                               if(err) return res.status(500).send({ message: 'Error al devolver publicaciones' });
                               if(!publications) return res.status(404).send({ message: 'No hay publicaciones' });

                               return res.status(200).send({
                                   total_items: total,

                                   publications,
                                   page: page,
                                   pages: Math.ceil(total / itemsPerPage)
                               });
                            });
                        });
          });
}

function getPublication(req, res){
    var publicationId = req.params.id;

    Publication.findById(publicationId, (err, publication) => {
        if(err) return res.status(500).send({ message: 'Error al buscar la publicación' });
        if(!publication) return res.status(404).send({ message: "No existe la publicación" });

        return res.status(200).send({ publication: publication });
    }).populate('user');
}

function deletePublication(req, res){
    var publicationId = req.params.id;

    Publication.find({ user: req.user.sub, _id: publicationId }).remove((err) =>{
        if(err) return res.status(500).send({ message: 'Error al borrar la publicación' });
        
        return res.status(200).send({ message: "Publicacion borrada correctamente" });
    });
}

/**
 * Subir archivos de avatar de usuarios
 */
function uploadFilePub(req, res) {
    var publicationId = req.params.id;

    if (req.files) {
        var file_path = req.files.image.path;
        // console.log(file_path);
        var file_split = file_path.split('\/');
        // console.log(file_split);

        var file_name = file_split[2];
        // console.log(file_name);

        var file_ext = file_name.split('\.')[1];
        // console.log(file_ext);
        
        if (file_ext == 'png' || file_ext == 'jpg' || file_ext == 'jpeg' || file_ext == 'gif') {
            Publication.findOne({ user: req.user.sub, '_id' : publicationId }).exec((err, publication) => {
                console.log(publication);
                if(publication){
                    // Actualizar documento de la publicación
                    Publication.findByIdAndUpdate(publicationId, {file: file_name},{new:true}, (err, publicationUpdated) => {
                        if (err) return res.status(500).send({ message: 'Error en la petición' });
                        if (!publicationUpdated) return res.status(404).send({ message: 'No se ha actualizado la publicación' });

                        return res.status(200).send({ publication: publicationUpdated });
                    });
                }else{
                    return removeErroFile(res,file_path, "Permisos insuficientes");
                }
            });
        } else {
            return removeErroFile(res,file_path, "Extension no válida");
        }

    } else {
        return res.status(200).send({ message: 'No se han subido imagenes' });
    }
}

function getImageFile(req, res){
    var image_file = req.params.imageFile;
    var path_file = './uploads/publications/'+image_file;

    fs.exists(path_file, (exists) => {
        if(exists){
            res.sendFile(path.resolve(path_file));
        }else{
            res.status(404).send({ 
                path: path_file,
                message: 'No existe la imagen' 
            });
        }
    });
}

function removeErroFile(res,file_path, msg){
    fs.unlink(file_path, (err) => {
        return res.status(200).send({ message: msg})
    });
}

module.exports = {
    prueba,
    savePublication,
    getPublications,
    getPublication,
    deletePublication,
    uploadFilePub,
    getImageFile,
}
