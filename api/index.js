'use strict'

var mongoose = require('mongoose');
var app = require('./app');
var conf = require('./services/config');
var port = 18755;

// Conexion DB
mongoose.Promise = global.Promise;
//mongoose.connect('mongodb://localhost:27017/curso_mean_social')
mongoose.connect('mongodb://'+conf.user+':'+conf.pass+'@'+conf.host+':27017/'+conf.collection, { useNewUrlParser: true})
    .then(() => {
        console.log('Conectado a la BDD realizado correctamente');
        // Crear servidor
        app.listen(port, () => {
            console.log('Servidor corriendo en http://localhost:'+port);
        });
    })
    .catch(() => {
        console.log('Error en la conexión');
    });