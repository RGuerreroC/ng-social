'use strict'

var express = require('express');
var MessageController  = require('../controllers/message');
var api = express.Router();

var mw_auth = require('../middlewares/authenticated');

api.get('/probando-md', mw_auth.ensureAuth, MessageController.prueba);
api.post('/message', mw_auth.ensureAuth, MessageController.saveMessage);
api.get('/my-messages/:page?', mw_auth.ensureAuth, MessageController.getReceivedMessages);
api.get('/messages/:page?', mw_auth.ensureAuth, MessageController.getEmittedMessages);
api.get('/unviewed-messages', mw_auth.ensureAuth, MessageController.getUnviewedMessages);
api.get('/set-viewed-messages', mw_auth.ensureAuth, MessageController.setViewedMessages);

module.exports = api;