import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { User } from '../../models/user';
import { UserService } from '../../services/user.service';

@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    providers: [ UserService ] 
})

export class LoginComponent implements OnInit{
    public title:string;
    public user: User;
    public status: string;
    public identity;
    public token;


    constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private _userService: UserService
    ){
        this.title = "Identificate";
        this.user = new User("", "", "", "", "", "", "ROLE_USER", "");
    }

    ngOnInit(){
        console.log('Componente de login cargado');
    }

    onSubmit(){
        // Logear usuario y recibir sus datos

        this._userService.signup(this.user).subscribe(
            response => {
                this.identity = response.user;
                if(!this.identity || !this.identity._id){
                    return this.status = "error";
                }else{
                    this.status = "success";
                    // Persistir datos
                    localStorage.setItem('identity', JSON.stringify(this.identity));
                    // Conseguir token
                    this.getToken();
                }
                
            },
            error => {
                var errorMessage = <any>error;
                console.log(errorMessage)
                if(errorMessage != null){
                    this.status = "error";
                }
            }
        );
    }
    
    getToken(){
        this._userService.signup(this.user, 'true').subscribe(
            response => {
                this.token = response.token;
                console.log(this.token);
                if(this.token.length <= 0){
                    return this.status = "error";
                }else{
                    this.status = "success";
                    // Persistir datos
                    localStorage.setItem('token', this.token);
                    // Conseguir contadores/estadisticas

                }
                
            },
            error => {
                var errorMessage = <any>error;
                console.log(errorMessage)
                if(errorMessage != null){
                    this.status = "error";
                }
            }
        );
    }
}